<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>
<div id="page" class="layout-flex">
  <div id="page-content">
    <div id="header-wrapper">
    <header id="header" role="banner">
      <?php if ($logo): ?>
        <div id="header-logo" class="header-item"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a></div>
      <?php endif; ?>
      <?php if ($site_name): ?>
        <div id="header-title" class="header-item"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></div>
      <?php endif; ?>
      <?php if ($site_slogan): ?>
        <div id="header-slogan" class="header-item"><?php print $site_slogan; ?></div>
      <?php endif; ?>
      <?php if ($page['header']): ?>
        <div id="header-content" class="header-item">
          <?php print render($page['header']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page['header_right']): ?>
        <div id="header-right" class="header-item">
          <?php print render($page['header_right']); ?>
        </div>
      <?php endif; ?>
      <?php if ($secondary_menu): ?>
        <nav id="header-menu" class="header-item" role="navigation">           
          <?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => array('links', 'inline', 'menu')))); ?>
        </nav>
      <?php endif; ?>
    </header>
    <?php if ($main_menu || $page['navigation']): ?>
      <div id="header-nav" class="clearfix">
        <?php if ($main_menu): ?>
          <nav id="header-nav-menu" role="navigation">
            <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'inline', 'menu', 'slimmenu')))); ?>
          </nav>
        <?php endif; ?>
        <?php if ($page['navigation']): ?><div id="header-nav-content"><?php print render($page['navigation']); ?></div><?php endif; ?>
      </div>
    <?php endif; ?>
    </div><!-- /header-wrapper -->
    <div id="main" role="main">
      <div id="main-content" class="column">
        
        <?php if ($messages): ?>
          <div id="messages"><?php print $messages; ?></div>
        <?php endif; ?>
        <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
        <?php print render($title_suffix); ?>
        
        <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
        <?php print render($page['content']); ?>
        <?php print $feed_icons; ?>

        <?php print $breadcrumb; ?>
      </div><!-- /main-content -->

      <?php if ($page['sidebar_first']): ?>
        <div id="sidebar-first" class="column sidebar">
          <?php print render($page['sidebar_first']); ?>
        </div><!-- #sidebar-first -->
      <?php endif; ?>

      <?php if ($page['sidebar_second']): ?>
        <div id="sidebar-second" class="column sidebar">
          <?php print render($page['sidebar_second']); ?>
        </div><!-- #sidebar-second -->
      <?php endif; ?>

    </div><!-- /main -->

    <?php if ($page['additional']): ?>
      <div id="additional"><div id="additional-content" class="column clearfix">
        <?php print render($page['additional']); ?>
      </div></div>
    <?php endif; ?>

    <div id="page-footer"></div>

  </div><!-- /page-content -->

  <footer id="footer" role="contentinfo">
    <div id="footer-content">
      <?php print render($page['footer']); ?>
    </div>
  </footer>

  <?php
    $collapser_title = "'";
    if ($logo) {
      $collapser_title .= '<div id="header-nav-logo"><a href="' . $front_page . '" title="' . t("Home") . '" rel="home"><img src="' . $logo . '" alt="'. t("Home") . '" /></a></div>';
    }
    if ($site_name) {
      $collapser_title .= '<div id="header-nav-title"><a href="' . $front_page . '" title="' . t("Home") . '" rel="home">' . $site_name . '</a></div>';
    }
    $collapser_title .= "'";
  ?>

  <script>
    (function ($) {
      $('ul.slimmenu').slimmenu( { resizeWidth: '465', collapserTitle: <?php print $collapser_title; ?> });
    })(jQuery);
  </script>

</div><!-- /page -->
