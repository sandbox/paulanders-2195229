Glook
=====

INTRODUCTION
------------
Simple clean responsive theme for content oriented sites.

REQUIREMENTS
------------
This module requires the following themes:
 * HTML5 (https://drupal.org/project/html5)

This module requires the following modules:
 * jQuery Update (https://drupal.org/project/jquery_update)

This module recomends the following modules:
 * Views Bootstrap (https://drupal.org/project/views_bootstrap)

INSTALLATION
------------
 * Install as usual, see
   https://drupal.org/getting-started/install-contrib/themes
   for further information.

TECHNOLOGIES
------------
 * CSS preprocessor - SASS (http://sass-lang.com)
 * SASS Framework - Compass (http://compass-style.org)
 * UI framework - Bootstrap (http://getbootstrap.com)
 * Syntax highlighter - Prism (http://prismjs.com)
 * Layout - Flex box model (http://www.w3.org/TR/css3-flexbox)

FEATURES
--------
 * Responsive
 * File types icons
 * Breadcrumbs at the bottom of page
